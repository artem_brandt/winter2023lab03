public class Cat {
	public String name;
	public String color;
	public int age;
	
	public void catPresent() {
		System.out.println("Surprise! You got a new " + color + " cat named " + name);
	}
	
	public void catAgeToHumanAge() {
		System.out.println("Wondering whether your cat is still in his youth or maybe is going through his midlife crisis? This method will help!");
		System.out.println(name + "'s age: " + age);
		System.out.println("In human years: " + (age * 7));
	}
}