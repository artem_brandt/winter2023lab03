import java.util.Scanner;

public class NationalPark {
	public static void main (String[] args) {
		Scanner sc = new Scanner (System.in);
		
		Cat[] clowder = new Cat[4];
		for (int i = 0; i < 4; i++) {
			clowder[i] = new Cat();
			System.out.println("What is the cat's name?");
			clowder[i].name = sc.next();
			System.out.println("What is it's color?");
			clowder[i].color = sc.next();
			System.out.println("What about the age?");
			clowder[i].age = sc.nextInt();
		}
		
		System.out.println("Info about the last cat in your clowder:");
		System.out.println("Name - " + clowder[3].name + ", Color - " + clowder[3].color + ", Age - " + clowder[3].age);
		
		clowder[0].catPresent();
		clowder[0].catAgeToHumanAge();
	}
}